
import smtplib
import inspect
import configparser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
# import email.utils
class Email:

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read(".env")
        

    def sendEmail(self,email_id1,email_id2,subject):
            from_address = self.config['email']['email_id']
            login        = from_address
            to_address   = [email_id1,email_id2]
            password     = self.config['email']['password']
            priority     ='2'
            try:
                msg = MIMEMultipart('alternative')

                server = smtplib.SMTP('smtp.gmail.com', 587)
                server.ehlo()  
                server.starttls()   
                server.login(login,password)  
                msg['Subject'] = subject
                msg['From'] = from_address
                msg['To'] = ', '.join(to_address)

                body = """\nBTS Butter......\nSide step right left to my beat...\nHigh like that moon..rock with me baby
                """

                msg.attach(MIMEText(body))

                with open('sample.txt', 'r') as file:
                    attachment = MIMEText(file.read())
                    file.close()
                    attachment.add_header(
                                            'Content-Disposition', 
                                            'attachment', 
                                            filename="sample.txt")           
                msg.attach(attachment)
                server.sendmail(from_address, to_address, msg.as_string())
                server.quit() 
                return True,"Mail sent"
            except Exception as e:
                print(e)
                return False,str(e)

# status,res = Email().sendEmail("soorya.prabhakaran@codcor.com","soorya2118@gmail.com","Test")
# print(status,res)